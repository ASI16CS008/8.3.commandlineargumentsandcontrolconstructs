student@administrator:~$ git config --global user.name "Anjana B"
student@administrator:~$ git config --global user.email "anjanabalub@gmail.com"
student@administrator:~$ git clone https://gitlab.com/ASI16CS008/8.3.commandlineargumentsandcontrolconstructs.git
Cloning into '8.3.commandlineargumentsandcontrolconstructs'...
warning: You appear to have cloned an empty repository.
Checking connectivity... done.
student@administrator:~$ cd 8.3.commandlineargumentsandcontrolconstructs
student@administrator:~/8.3.commandlineargumentsandcontrolconstructs$ touch README.md
student@administrator:~/8.3.commandlineargumentsandcontrolconstructs$ git add README.md
student@administrator:~/8.3.commandlineargumentsandcontrolconstructs$ git commit -m "add README"
[master (root-commit) 42a477d] add README
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 README.md
student@administrator:~/8.3.commandlineargumentsandcontrolconstructs$ git push -u origin master
Username for 'https://gitlab.com': ASI16CS008
Password for 'https://ASI16CS008@gitlab.com': 
Counting objects: 3, done.
Writing objects: 100% (3/3), 215 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://gitlab.com/ASI16CS008/8.3.commandlineargumentsandcontrolconstructs.git
 * [new branch]      master -> master
Branch master set up to track remote branch master from origin.
student@administrator:~/8.3.commandlineargumentsandcontrolconstructs$ git add exp3.odt
student@administrator:~/8.3.commandlineargumentsandcontrolconstructs$ git commit -m "add exp3"
[master 6411812] add exp3
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 exp3.odt
student@administrator:~/8.3.commandlineargumentsandcontrolconstructs$ git push -u origin master
Username for 'https://gitlab.com': ASI16CS008
Password for 'https://ASI16CS008@gitlab.com': 
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 120.79 KiB | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://gitlab.com/ASI16CS008/8.3.commandlineargumentsandcontrolconstructs.git
   42a477d..6411812  master -> master
Branch master set up to track remote branch master from origin.
